The RuleReserve Network Identifies Rules That Are ‘In Effect’ and ‘Applicable’ 

When complexity is externalized to the edge of the oughtomation network, the
RuleReserve network can be optimized for high-volume, high-speed sifting of its
general collection of rules, for those which are ‘in effect’ and
‘applicable’. Each time an `is.xa` key message arrives from a RuleTaker
component, the RuleReserve network employs it as a sifting screen to its
`rulereserve.xa` data store. It returns an `ought.xa` message containing only
rules deemed to be ‘in effect’ for the documented temporal and jurisdictional
context, and ‘applicable’ to the documented particulars. The RuleReserve network
does not process rule logic, as that is performed ‘at the edge’ on RuleTaker
components.

Free/libre/open Internet access to the latest comprehensive `rulereserve.xa`
file provides anyone the ability to set up and operate a Superset or Subset
RuleReserve node. The default node configuration includes the user’s own Subset
RuleReserve cache. The `rule.xa` and `lookup.xa` data is stored by each node
within a consolidated [m x n] matrix (i.e. m rows x n columns), where the data
of each indexed row is arranged like a long tape on which one `rule.xa` or a
`lookup.xa` record is splayed out horizontally. Each `is.xa` message is
similarly strucured as a single row, so that it is preconfigured to work as a
data seive to sift through all accessible holdings of the RuleReserve network,
swiftly and efficiently. The `rule.xa` records which are not ‘in effect’ and are
not ‘applicable’ will drop out of view .

Our sifting procedure uses primitive signal correspondence rather than
sophisticated semantic pattern matching, so it is trivial and fast. Each
incoming `is.xa` single row request message contains a header with some
addressing and user preference data so that the response message can be sent
back correctly. Otherwise it consists of subject-predicate-object triples in
JSON, the colon symbol ‘:’ signifying a predicate relation.

A RuleReserve node immediately compares the `is.xa` metadata columns with
similarly-structured metadata columns of all m rows of discributed
`rulereserve.xa` data. The first column of each row contains the universal
unique identifier (UUID), and the first two dozen or so columns are assigned to
`rule.xa` metadata. The rows that will be retained by the `is.xa` seive that are
the ones whose metadata columns of subject-predicate-object triples correspond
to all of the availabe subject-predicate-object triples contained in the
seive. RuleReserve sifts its entire holdings accessible to the `is.xa`, first by
comparing the columns for date/time and jurisdiction context data. Each time
these correspond in a particular row, that `rule.xa` is deemed to be ‘in effect’
for the GIVEN context, and its UUID is retained in memory for the second sifting
operation. The second sifting operation is performed on all the remaining
metadata columns. Rows are retained which have all of the available
subject-predicate-object triples contained in the seive, representing all the
rules that are ‘in effect’ and ‘applicable’ to the particular circumstance
documented in the originating `is.xa` message. RuleReserve uses the UUIDs in
memory to assemble an `ought.xa` response with the full data of all the rows in
the same horizontal ‘tape’ format as they are stored on the RuleReserve
network. Additional chained `rule.xa` and `lookup.xa` tables referred to with
any of these sifted rules can be included in this `ought.xa` message, or not,
which is a user preference indicated in the header of the originating
`is.xa`. Some users may prefer to make those indirect queries over the network
on an as-needed basis, especially when `lookup.xa` reference files may be very
large. Others users may prefer to replicate all data that they depend upon into
their own RuleReserve node.

A human or machine user of a RuleTaker component, upon receiving an `ought.xa`
message from the RuleReserve network, may wish to run an additional sifting
operation with additional data in subject-predicate-object triples that are held
locally. This is straightforward to accomplish because the default deployment
configuration for every oughtomation node includes all three functional
components: RuleMaker, RuleReserve and RuleTaker. There are two main reasons a
rule taker would run an additional sifting process using their own RuleReserve
node(s):

1. The oughtomation specification does not assume that rule takers want to
   expose all of their relevant information to the wider RuleReserve network. An
   astute rule taker is presumed to expose just enough information to the
   network as required to obtain a manageable shortlist of rules that can then
   be processed efficiently on their own computing resources, using their
   private data.
1. Rule takers will normally apply filters against spam, malicious and junk
   rules. The oughtomation specification does not assume that users would want
   Superset RuleReserve network administrators wield such power over
   filtering. Artificial naïvety (A∅) keeps filtering under end-user
   control. Instead, users who prefer to delegate this function will support a
   competitive decentralized market for Subset RuleReserve filtering algorithms
   and services.
    
    
Once all of the ‘in effect’ and ‘applicable’ sifting is done by the RuleReserve
network, it’s the job of the RuleTaker component to process the logic data of
the `rule.xa` records received in an `ought.xa` message. RuleReserve nodes do
not process the logic tables because our artificial naïvety (A∅) design defaults
to keeping knowledge at the edge under end-user control, and mimizing network
operator capabilities. Data processing that is particular to a circumstance is
appropriate for a RuleTaker component to perform, or for the user to allocate
for processing on any other software platform they prefer.  The RuleReserve
network can only do storage; simple sifting for ‘in effect’ and ‘applicable’;
and request-response messaging:

- distibuted storage of `rule.xa` records on a `rulereserve.xa` fabric;
- metadata sifting that applies each `is.xa` as a sieve to reduce
  `rulereserve.xa` to `ought.xa` collections;
- messaging that receives `is.xa` requests, and sends `ought.xa` responses;


The RuleTaker Component Identifies the Rule Assertions to be ‘Invoked’ Whereas
the distributed and decentralized RuleReserve network is well suited to
high-volume sifting of a large general collection of rules to discover those
which are ‘in effect’ and ‘applicable’ to a circumstance, the RuleTaker
component is best suited to resolve the logic expressed within the sifted
selection of `rule.xa` records in order to winnow these to the particular
normative assertions ‘invoked’ by the given circumstance.  The logic table of
each `rule.xa` record expresses two or more scenarios involving pairs of Input
Conditions and Output Assertions, and these scenarios must be assessed in
relation to circumstance-specific descriptors within an `is.xa` message.

Below we step through the method by which a RuleTaker component processes ‘rules-as-data’. 

## Start

This sequence begins when a RuleReserve node returns an `ought.xa` response
message to the RuleTaker component that issued the `is.xa` request message. That
`ought.xa` response contains the results of two sifting procedures that screened
the `rulereserve.xa` compendium for only those `rule.xa` rows which are deemed
by their authors to be ‘in effect’ and ‘applicable’ to the data contained in the
originating `is.xa` request message.

## Log

The moment RuleTaker recieves an `ought.xa` response message, it logs metadata
about the receipt and checks that the referenced `is.xa` message is identical to
one of those that it issued. If so it proceeds, and if not RuleTaker logs
available metadata about the unmatch message (see “Notify” below), deletes it,
and terminates the session.

## Check

A `rule.xa` row within the `ought.xa` message may contain a complete normative proposition. Another may identify with their UUID hashes any external `lookup.xa` records, or other forward and/or backward chained `rule.xa` records that it depends upon. The RuleReserve should have supplied these, but if any are missing, the RuleTaker sends a supplementary `is.xa` request message to the RuleReserve network. If any of these remain unfound, an alert is logged. (See “Alert” below.) 

## Focus

RuleTaker will treat as subject-predicate-object triples each pair of SubjectNoun and ObjectDescriptor columns in the logic table of each `rule.xa` row of the message. Here are some examples that illustrate how these two syntactic elements focus attention on the most essential data:

1. The subject-predicate-object relation  "box”:"standard" is readily obtained from the SubjectNoun and ObjectDescriptor of one of our earlier sample sentences:
      ```"sentence": {
        "Determiner":"This",
        "SubjectNoun":"box,
        "PastParticipleVerb":"as measured",
        "AuxiliaryVerb":"is of",
        "Object Descriptor:"standard",
        "ObjectNounOrVerb":"type"
      }```
1. RuleTaker would see "number_of_residential_properties”:"2” in the following:
      ```"sentence": {
        "Determiner":"The”,
        "SubjectNoun":"number of residential properties”,
        "PastParticipleVerb":"currently registered to”,
        "ObjectNounOrVerb":"the purchaser”,
        "AuxiliaryVerb":"is”,
        "Object Descriptor:"2”
      }```
1. The relation  "country_of_residence”:"Rwanda” is formed from this sentence:
      ```"sentence": {
        "Determiner":"The”,
        "SubjectNoun":"country of residence”,
        "PastParticipleVerb":"deemed habitual”,
        "ObjectNounOrVerb":"of the person”,
        "AuxiliaryVerb":"is”,
        "Object Descriptor:"Rwanda”
      }```

## Tolerate

Due to the oughtomation design employing a single consistent set of syntactic
elements for rule expression, it is feasible for the application to construct
relevant subject-predicate-object data packages equivalently in any language in
any character set read left-to-right or right-to-left, independently of the
order in which elements are provided in any particular sentence. Supporting
words that enhance linguistic style can be included with these natural language
elements without loss of utility. This may initially appear to leave far too
much to chance when matching words and phrases among disparate users in diverse
contexts, entities and perspectives. However two methods pull in the direction
of making `rule.xa` records more usable, and helping `is.xa` request messages to
return more complete `ought.xa` responses.

### Utility of Alignment to Semantic Standards

The simplest an fastest test involves, of course, exact equivalency with
standardized lexicons: "box":"standard"=="box”:"standard". To the extent rule
authors and the designers of application data structures become aware of and
acquire an interest in having these subject-predicate-object triples
auto-generated in the background in an intelligible way, they will be attracted
by the efficiencies easily obtained by migrating towards shared terminology,
such as Semantic Web structured data schemas and their respective extension
guidelines negotiated over the past quarter century across many specialized
communities of practice. (Berners-Lee et al., 2001)

### Use of Lookups for Data Mapping

Tolerance of expressive diversity is facilitated with various rudimentary
techniques, such as accepting partial strings, or using `lookup.xa` tables of
synonyms and/or multi-language dictionaries. So data can “correspond” without
having to “match” characters directly:
"box":"standard"=="cajas":"estandarizados". With effective use of these methods,
each rule maker and rule taker retains their autonomous prerogative to use the
terminology they prefer, and in some contexts they may intentionally distinguish
their terminology from a standard lexicon. Even where standards are available,
data mapping lookups are required whenever two or more overlapping schemas
employ different categories and terms (e.g. UN/CEFACT and UBL). (Parry et al.,
2010)

## Winnow

The RuleTaker component now performs a repetitive cycle in order to compare each
subject-predicate-object triple of an `is.xa` request message with each of the
subject-predicate-object triples arising from the Input Condition sentences of
each `rule.xa` row of the corresponding `ought.xa` response message. The outcome
of this winnowing sequence is to isolate only the Output Assertion(s) from all
the ‘in effect’ and ‘applicable’ rules which rule authors deem to be ‘invoked’
by the particulars of the documented circumstance. It is useful to keep in mind
that at this stage we’re only using two of the six syntactic element columns
contained in an `ought.xa` message for each Input Condition sentence of its
`rule.xa` logic tables. Those cells may contain alphanumeric data of any
character set. Next to these are the columns which document all the scenario
combinations, and each of these cells is populated with one of the four
available two-bit values `{00,01,10,11}`.

## Scan—Sift—Assess

The RuleTaker component can be configured to perform the following Scan, Sift
and Assess operations in sequence or in parallel, with each
subject-predicate-object triple from the `is.xa` message. This whole procedure
must assess all the `is.xa` subject-object triples against all scenarios of all
the Input Conditions of all the `rule.xa` rows of the `ought.xa` message.

### Scan

RuleTaker picks up the first subject-predicate-object triple from the `is.xa`
message and scans row-by-row vertically down all of the subject-predicate-object
triples formed by the “SubjectNoun”:“ ObjectDescriptor” elements of that sifted
collection of `rule.xa` rows in the `ought.xa` message, testing for
corresponding subject-predicate-object triples.

### Sift

Each time the subject-predicate-object triple from an `is.xa` message corresponds with a subject-predicate-object triple from a `rule.xa` row in the `ought.xa` message, RuleTaker reads the values {00,01,10,11} from all of the scenario columns that correspond to that Input Condition of that `rule.xa` row. Whenever the Input Condition value is “00”, those scenarios are dropped from further consideration in relation to that entire `rule.xa` row. This is because the “00” signal for “inapplicable” (i.e. the data elements do not correspond) contradicts the established fact that the two data elements currently being compared do correspond. 

### Assess

At the end of the above operation with a single subject-predicate-object triple from the `is.xa` message, there should remain in consideration from the logic tables of all the ‘in effect’ AND ‘applicable’ `rule.xa` rows, only the Input Condition scenarios from the logic tables that inform us which of their Output Assertions are ‘invoked’. But there are a few possible outcomes to take into account. When either (a) or (b) occur, RuleTaker proceeds to Evaluate. But when neither (a) or (b) occur, RuleTaker first proceeds to Alert, before continuing to Evaluate.
                (a) When the only remaining Input Conditions under consideration contain the signals “01” or “10”, there must be exactly one remaining scenario per `rule.xa` row of the `ought.xa` message. 
                (b) When one or more of the remaining Input Conditions under consideration contains the signal “11”, there may be up to that many more scenarios remaining beyond one scenario per `rule.xa` row.
                (c) When any `rule.xa` row of the `ought.xa` message ends up with no scenarios remaining in consideration, RuleTaker flags an error in the metadata or in the logic of the `rule.xa` record. This is because the previous RuleReserve sifting operation, any additional sifting operations performed by the end user, determined this rule to be ‘in effect’ and ‘applicable’ based on data provide in the current `is.xa` message. When `rule.xa` records are assembled into an `ought.xa` collection, RuleTaker requires at least one Output Assertion from each rule to be ‘invoked’ for the circumstance documented in the originating `is.xa` message. 
                (d) When any `rule.xa` row of the `ought.xa` message ends up with more than one scenario that contains only values “01” and/or “10”, and the sets of Output Assertions are identical, RuleTaker flags a `rule.xa` inefficiency, since two or more columns of its logic table could be consolidated into one column. Such an outcome is logical and usable, however it is either a sloppy rule, or this situation is indirect evidence of an error. 
                (e) When any `rule.xa` row of the `ought.xa` message ends up with more than one scenario containing only values “01” and/or “10”, and the sets of Output Assertions are different, RuleTaker flags an error in `rule.xa` logic. If the Rule Author wanted to express more than a single outcome, a value of “11” should have appeared in at least one of the remaining scenarios.)
                
## Alert (if required)

Each error and inefficiency cited above will cause RuleTaker (if permitted by the user) to log an alert with generic information to a RuleReserve node. A background RuleReserve network service periodically scans for error and inefficiency notices in the log files, and generates automated messages to the Managers and Authors listed in `rule.xa` metadata.

## Reflect

At this point it seems appropriate to remind the reader that oughtomation is premised upon human-centred automation, meaning that anyone receiving an `ought.xa` message retains responsibility to assure that the normative data obtained in this way is sufficiently definitive and sufficiently valid. The normative data recieved through oughtomation necessarily embodies some uncertainty, and person who is deemed to be subject to a rule ultimately retains their inalienable prerogative of discretion about whether or not, and to what degree, to act in accordance with it. An end user may prefer to outsource, automate and/or modify the data they obtain through oughtomation, in light of their available resources and competencies, but the oughtomation system is, by design, a naïve data transmission service.
Therefore let’s re-cap what has occurred to this point in the oughtomation sequence. Various rule authors described rules with passive structured data comprised of some metadata and a logic gate. The logic gate consists of a bilattice that relates structured natural language to sets of four symbols {0,1,10,11}|{0,1,10,11} that classify four Input Conditions, and four Output Assertions. They publish these `rule.xa` records online in a freely accessible, decentralized network service with a distributed `rulereserve.xa` library that is capable of only three functions: store, sift and message. When an extermal agent sends an `is.xa` request message, it is used to sift through the metadata of all the [rules.xa] rows of the `rulereserve.xa` library, so that they almost instantaneously get back an `ought.xa` response message with a bounded set of rules that are deemed to be ‘in effect’ and ‘applicable’ to the circumstance described in the `is.xa` message. The agent then applies any available data they prefer to the logic gate in order to winnow the Input Conditions down to a discrete set of Output Assertions which are deemed to be ‘invoked’ by the particular circumstance described in the `is.xa` message. Along the way, any detectable errors and inefficiencies of rule expression have be flagged, and their authors have been alerted.
